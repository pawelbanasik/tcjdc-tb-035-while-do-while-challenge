
public class App {

	public static void main(String[] args) {

		// int count = 1;
		// while (count != 6) {
		//
		// System.out.println("Count value is " + count);
		// count++;
		// }

		// for (int i = 0; i < 5; i++) {
		// System.out.println("Count value is " + i);
		//
		// }

		// count =1;
		// while (true) {
		//
		// if(count==6) {
		// break;
		// }
		// System.out.println("Count value is " + count);
		// count++;
		// }

		// count = 6;
		// do {
		// System.out.println("Count value is " + count);
		// count++;
		// if(count > 100) {
		//
		// break;
		// }
		// } while (count != 6);

		int start = 5;
		int finish = 20;
		int count = 0;

		while (start <= finish) {
			if (!isEvenNumber(start)) {
				start++;
				continue;

			}
			count++;

			System.out.println("Number " + start + " is an even number.");

			start++;
			if (count == 5) {
				break;

			}

		}
		System.out.println("Total of even numbers is " + count);

	}

	public static boolean isEvenNumber(int number) {

		if (number % 2 == 0) {

			return true;
		} else {
			return false;
		}
	}
}
